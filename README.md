# pdf-overlay-last

This utility accepts a PDF (you can drag & drop, or run it and it will ask) which it will then take the last page and overlay the file "vat_exemption_signature_template.pdf" from your "documents" folder. The result is saved in the same place as the source document, but "-lastpage-signed.pdf" is added to the filename.

You can create "vat_exemption_signature_template.pdf" from the similarly named word document which you should find in the same place you found this readme file. Simply open the document, put a signature of some description in the box and remove everything else. Then, "File" > "Save as..." and choose PDF file format.
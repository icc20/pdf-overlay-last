#!/usr/bin/env python
"""
Find the last page of a document, overlay a signature
"""

import PyPDF2
import os
import sys
import ctypes.wintypes
import tkinter, tkinter.filedialog, tkinter.messagebox

def get_my_docs():
    # pinched from https://stackoverflow.com/questions/3858851/python-get-windows-special-folders-for-currently-logged-in-user
    CSIDL_PERSONAL=5
    SHGFP_TYPE_CURRENT=0
    buf=ctypes.create_unicode_buffer(ctypes.wintypes.MAX_PATH)
    ctypes.windll.shell32.SHGetFolderPathW(0, CSIDL_PERSONAL, 0, SHGFP_TYPE_CURRENT,buf)
    return buf.value
    

def do_merge(file_in, file_sig):
    file_out=file_in.replace(".pdf","-lastpage-signed.pdf")

    with open(file_in, "rb") as fh_input:
        pdf_in=PyPDF2.PdfFileReader(fh_input)
        page_last=pdf_in.getPage(pdf_in.getNumPages()-1)
        
        with open(file_sig,"rb") as fh_sig:
            print(f"Reading signature PDF from {file_sig}")
            pdf_sig=PyPDF2.PdfFileReader(fh_sig)
            page_sig=pdf_sig.getPage(0)
            
            pdf_out=PyPDF2.PdfFileWriter()

            page_last.mergePage(page_sig)
            pdf_out.addPage(page_last)
            with open(file_out, "wb") as fh_out:
                print(f"Writing to {file_out}")
                pdf_out.write(fh_out)
                tkinter.messagebox.showinfo(title="Done",message=f"Output written to {file_out}")

SIGNATURE_FILE="vat_exemption_signature_template.pdf"

if __name__=="__main__":
    tkroot=tkinter.Tk()
    tkroot.withdraw()
    file_sig=os.path.join(get_my_docs(),SIGNATURE_FILE)
    if not os.path.exists(file_sig):
        tkinter.messagebox.showerror("Error",f"Did not file signature file: {file_sig}. Perhaps you need to create this?")
        exit(1)
    file_in=""
    if len(sys.argv) > 1 and os.path.exists(sys.argv[1]):
        file_in=sys.argv[1]
        do_merge(file_in,file_sig)
    else:
        file_in=str(tkinter.filedialog.askopenfilename(title="Select file to extract and sign",filetypes=(("PDF files","*.pdf"),("All files","*.*"))))
        if os.path.exists(file_in):
            do_merge(file_in,file_sig)
   
